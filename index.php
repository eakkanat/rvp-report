
<html>
  <head>
      <title>RVP-Report</title>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>

  </head>
  <body>
    <div class="container mt-3 mb-5">
      <h2 class="p-3 text-white shadow-sm border-primary" style="background:#6f42c1;">Report form</h2>
          <!--  action="/action_page.php" -->
       <div class="col-md-12">
          <label for="title">Title:</label>
          <input type="text" class="form-control -default" id="title" placeholder="Enter title" name="title">
      </div>
      <div class="col-md-12">
         <label for="environment">Environment:</label>
          <select class="form-select" id="environment">
            <option>Production</option>
            <option>UAT</option>
            <option>SIT</option>
          </select>
      </div>
       <div class="col-md-12">
        <label for="functionvalue">Function:</label>
          <select class="form-select" id="functionvalue">
            <option>แจ้งเหตุ</option>
            <option>เดินราวด์</option>
            <option>คำสั่งการ</option>
           </select>
       </div>
       <div class="col-md-12">
          <label for="platform">Platform:</label>
          <select class="form-select" id="platform">
          <option>iOS</option>
          <option>Android</option>
          <option>Web Office</option>
          </select>
        </div>
        <div class="col-md-12">
          <label for="detail">Detail:</label>
          <textarea class="form-control" rows="5" id="detail" placeholder="Enter detail" name="detail"></textarea>
        </div>
        <div class="col-md-12">
          <label for="comment">Comment:</label>
          <textarea class="form-control" rows="5" id="comment" placeholder="Enter comment" name="comment"></textarea>
        </div>
        <div class="col-md-12">
          <label for="comment2">Comment2:</label>
          <textarea class="form-control" rows="5" id="comment2" placeholder="Enter comment" name="comment2"></textarea>
        </div>
        <div class="col-md-12">
          <label for="comment3">Comment3:</label>
          <textarea class="form-control" rows="5" id="comment3" placeholder="Enter comment" name="comment3"></textarea>
        </div>
        <div class="col-md-12">
          <label for="comment4">Comment4:</label>
          <textarea class="form-control" rows="5" id="comment4" placeholder="Enter comment" name="comment4"></textarea>
        </div>
        <div class="col-md-12">
          <input type="file" id="files" accept="image/png, image/jpeg" name="files[]" multiple />
          <div class="row" id="images">
          </div>
        </div>
        <div class="col-md-12" align="center">
            <button type="button" class="btn btn-success" onclick="userAdd()">Submit</button>
        </div>
    </div>
  </body>

  <script>
      var service_path = "https://dc25-2001-fb1-b1-4068-8e0-62c3-814f-752e.ngrok.io";
      var imageList = [];
      
      function userAdd() {
          var xhttp = new XMLHttpRequest();
          var title = document.getElementById("title").value;
          var environment = document.getElementById("environment").value;
          var functionvalue = document.getElementById("functionvalue").value;
          var platform = document.getElementById("platform").value;
          var detail = document.getElementById("detail").value;
          var comment = document.getElementById("comment").value;
          var comment2 = document.getElementById("comment2").value;
          var comment3 = document.getElementById("comment3").value;
          var comment4 = document.getElementById("comment4").value;
          //var image = document.getElementById("image").value;

          getImageThumb();

          const textJson = {
            "title" : title,
            "environment" : environment,
            "functionvalue" : functionvalue,
            "platform" : platform,
            "detail" : detail,
            "comment" : comment,
            "comment2" : comment2,
            "comment3" : comment3,
            "comment4" : comment4,
            "image" : imageList
          }
          //var text = document.getElementById('text').value;
          xhttp.onreadystatechange = function() {
               if (this.readyState == 4 && this.status == 200) {
                 var resText = this.responseText
                 var resJson = JSON.parse(resText);
                 alert("id : "+resJson['insertId']);
                  location.reload();
               }
          };
          xhttp.open("POST", service_path+"/report/add", true);
          xhttp.setRequestHeader("Content-type", "application/json");
          xhttp.send(JSON.stringify(textJson));
      }

      function userGet() {
          var xhttp = new XMLHttpRequest();
          var getId = document.getElementById("getId").value;
          const textJson = {
            "id" : getId
          }
          xhttp.onreadystatechange = function() {
               if (this.readyState == 4 && this.status == 200) {
                 var resText = this.responseText;
                 var resJson = JSON.parse(resText);
                 alert("text : "+resJson['text']);
                 document.getElementById('getId').value = '';
               }
          };
          xhttp.open("POST", service_path+"/get", true);
          xhttp.setRequestHeader("Content-type", "application/json");
          xhttp.send(JSON.stringify(textJson));
      }

     $(document).ready(function() {
        if (window.File && window.FileList && window.FileReader) {
          var imageListSize = document.getElementsByClassName("imageThumb");
          $("#files").on("change", function(e) {
            var files = e.target.files,
            filesLength = files.length;
            var filesTotal = (imageListSize.length + filesLength);
            var maxAddImage = 10;

            if(filesTotal <= maxAddImage){
              for (var i = 0; i < filesLength; i++) {
                var f = files[i]
                var fileReader = new FileReader();
                fileReader.onload = (function(e) {
                    var file = e.target;
                    var result = e.target.result;

                    var mainElementDiv = document.getElementById('images');

                    var img = document.createElement('img');
                    img.src = result;
                    img.title = file.name;
                    img.className = 'imageThumb img-thumbnail';

                    var button = document.createElement('button');
                    button.className = "btn btn-danger mt-3 mb-3";
                    button.id = "remove-image";
                    button.textContent = "ลบรูปภาพนี้";
                    button.onclick = function () {
                      $(this).parent("#pip").remove();
                      getSize();
                    };

                    var childDivAddImage = document.createElement('div');
                    childDivAddImage.id = "pip";
                    childDivAddImage.className = "col-lg-4 mb-4 mb-lg-0";
                    childDivAddImage.align = "center";

                    childDivAddImage.appendChild(img);
                    childDivAddImage.appendChild(document.createElement("br"));
                    childDivAddImage.appendChild(button);

                    mainElementDiv.appendChild(childDivAddImage);
                });

                fileReader.readAsDataURL(f);
              }
            }else{
              alert("Maximam "+maxAddImage+" images");
            }
          });
        } else {
          alert("Your browser doesn't support to File API");
        }
      });

     function getSize(){
        var imageListSize = document.getElementsByClassName("imageThumb");
        console.log("Get Size : "+imageListSize.length);
     }

     function getImageThumb(){
        imageList = [];
        var imageListSize = document.getElementsByClassName("imageThumb");

        for(var i = 0 ; i < imageListSize.length ; i++){
          imageList.push(imageListSize[i].src);
        }
        
        console.log("Images send to service : "+imageList.length);
    }
  </script>

</html>
